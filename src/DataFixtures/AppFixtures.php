<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use Faker;

use App\Entity\Produit;
use App\Entity\Marque;
use App\Entity\Fournisseur;
use App\Entity\Utilisateur;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 2; $i++) {
            $fournisseur = new Fournisseur();
            $fournisseur->setNom('fournisseur '.$i);
            $manager->persist($fournisseur);
            for($j = 0; $j < 3; $j++){
                $marque=new Marque();
                $marque->setNom('marque '.$i.' '.$j);
                $marque->setFournisseur($fournisseur);
                $manager->persist($marque);
                for($k = 0; $k < 10 ; $k++){
                    $produit=new Produit();
                    $produit->setTitre('produit '.$i.' '.$j.' '.$k);
                    $produit->setDescription($faker->text);
                    $produit->setQuantite(mt_rand(0, 100));
                    $produit->setPrix(mt_rand(2000, 5000));
                    $produit->setGenre($faker->asciify('genre-****'));
                    $produit->setType($faker->asciify('type-****'));
                    $produit->setMarque($marque);
                    $manager->persist($produit);
                }
            }
        }
        $utilisateur= new Utilisateur();
        $utilisateur->setPassword('$argon2id$v=19$m=65536,t=4,p=1$S2s4SjB6Mi5UWGQ5ZUFlSw$5zby1krSPFs5luzewPdXCBQtiQmcY8M+1GranoF/wMQ');
        $utilisateur->setEmail('admin@admin.com');
        $utilisateur->setRoles(['ROLE_ADMIN']);
        $utilisateur->setNom('admin');
        $utilisateur->setPrenom('admin');
        $utilisateur->setAdresse('admin adresse');
        $utilisateur->setTelephone('+5156223233');
        $manager->persist($utilisateur);

        $utilisateur= new Utilisateur();
        $utilisateur->setPassword('$argon2id$v=19$m=65536,t=4,p=1$S2s4SjB6Mi5UWGQ5ZUFlSw$5zby1krSPFs5luzewPdXCBQtiQmcY8M+1GranoF/wMQ');
        $utilisateur->setEmail('user@user.com');
        $utilisateur->setRoles([]);
        $utilisateur->setNom('User');
        $utilisateur->setPrenom('Test');
        $utilisateur->setAdresse('user adresse');
        $utilisateur->setTelephone('+5156223234');
        $manager->persist($utilisateur);

        $manager->flush();
    }
}
