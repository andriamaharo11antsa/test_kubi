<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Entity\Commande;
use App\Entity\CommandeProduit;


use App\Form\CommandeProduitFormType;
use App\Repository\ProduitRepository;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\FournisseurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ProduitRepository $produitRepository , FournisseurRepository $fournisseurRepository): Response
    {

        return $this->render('home/index.html.twig', [
            'produits' => $produitRepository->findAll()
        ]);
    }


    /**
     * @Route("/produit/{id}", name="produit")
     */
    public function show(Request $request, Produit $produit, EntityManagerInterface $em): Response
    {

        $commandeProduit = new CommandeProduit();

        $form = $this->createForm( CommandeProduitFormType::class, $commandeProduit);

        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()){
            //dd($form->get('quantite')->getData());
            $manager = $this->getDoctrine()->getManager();
            $repo = $manager->getRepository(Commande::class);
            $repo2 = $manager->getRepository(CommandeProduit::class);

            $commande = $repo->findOneBy([
                'utilisateur' => $this->getUser(),
                "status" => 0
            ]);

    
            if(is_null($commande)){
                $commande = new Commande();
                $commande
                    ->setUtilisateur($this->getUser())
                    ->setDate(new \Datetime())
                    ->setStatus(0)
                    ;
            }
            $commandeProduitfind=$repo2->findOneBy([
                'produit' => $produit,
                "commande" => $commande
            ]);
            if(!is_null($commandeProduitfind)){
                $commandeProduit=$commandeProduitfind;
                $commandeProduit->setQuantite( $commandeProduit->getQuantite() + $form->get('quantite')->getData() );
            }
            

            if($commandeProduit->getquantite() == $form->get('quantite')->getData()){
                $commandeProduit->setProduit($produit);
                $commandeProduit->setCommande($commande);

                $commande->addCommandeProduit($commandeProduit);
            }
            

            $manager->persist($commande);
            $manager->persist($commandeProduit);

            $manager->flush();

            $this->addFlash('success', 'Le produit est ajouté à votre commande actuel');

        }


        return $this->render('home/produit.html.twig', [
            'produit' => $produit, 
            'commandeForm'=>$form->createView()
        ]);
    }

}
