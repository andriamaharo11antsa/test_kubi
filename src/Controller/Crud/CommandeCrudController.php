<?php

namespace App\Controller\Crud;

use App\Entity\Commande;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CommandeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Commande::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->setLabel('Numero de la commande'),
            AssociationField::new('utilisateur')
                ->setRequired(true)
                ->setTemplatePath('admin/field/commande_utilisateur.html.twig'),
            DateTimeField::new('date'),
            ChoiceField::new('status')->setChoices(['Pas encore valider' =>0, 'Valider'=>1]),
            AssociationField::new('commandeProduits')->setTemplatePath('admin/field/commande_produit.html.twig')->setLabel('Détails'),
        ];
    }
    

    public function configureActions(Actions $actions): Actions
    {
        return $actions
        ->disable(Action::NEW, Action::EDIT , Action::DELETE )
        
        ;
    }
}
