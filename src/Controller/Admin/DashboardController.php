<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Commande;
use App\Entity\Fournisseur;
use App\Entity\Marque;
use App\Entity\Produit;
use App\Entity\Utilisateur;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Testkubi');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Tableau de bord', 'fa fa-home'),
            MenuItem::linktoRoute('Voir le site', 'fa fa-arrow-left ', 'home'),

            MenuItem::section('Produits'),
            MenuItem::linkToCrud('Produits', 'fa fa-car', Produit::class),
            MenuItem::linkToCrud('Marques', 'fa fa-tags', Marque::class),
            MenuItem::linkToCrud('Fournisseurs', 'fa fa-file-text', Fournisseur::class),

            MenuItem::section('Utilisateurs'),
            //MenuItem::linkToCrud('Administrateurs', 'fa fa-key', Utilisateur::class),
            MenuItem::linkToCrud('Utilisateurs', 'fa fa-user', Utilisateur::class),
            MenuItem::linkToCrud('Commandes', 'fa fa-list-alt', Commande::class),
        ];
    }
}
