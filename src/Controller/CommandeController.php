<?php

namespace App\Controller;

use Exception;
use App\Entity\Commande;
use App\Entity\CommandeProduit;
use App\Form\CommandeProduitType;
use App\Repository\CommandeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommandeController extends AbstractController
{
    /**
     * @Route("/commande", name="commande")
     */
    public function index(CommandeRepository $commandeRepository , Request $request): Response
    {
        $commandes=$commandeRepository->findBy([
            'utilisateur'=>$this->getUser()
        ]);

        return $this->render('commande/index.html.twig', [
            'commandes'=>$commandes
        ]);
    }
    /**
     * @Route("commande/{id}", name="commande_produit_edit", methods={"POST"})
     */
    public function edit(Request $request, CommandeProduit $commandeProduit){
        if ($this->isCsrfTokenValid('edit'.$commandeProduit->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            if($request->request->get('quantite')==0){
                $entityManager->remove($commandeProduit);
            }else{
                $commandeProduit->setQuantite($request->request->get('quantite'));
                $entityManager->persist($commandeProduit);
            }
            $entityManager->flush();
        }
        return $this->redirectToRoute('commande', [], Response::HTTP_SEE_OTHER);
    }
    /**
     * @Route("commandeD/{id}", name="commande_produit_delete", methods={"POST"})
     */
    public function delete(Request $request, CommandeProduit $commandeProduit): Response
    {
        if ($this->isCsrfTokenValid('delete'.$commandeProduit->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($commandeProduit);
            $entityManager->flush();
        }

        return $this->redirectToRoute('commande', [], Response::HTTP_SEE_OTHER);
    }
    /**
     * @Route("commandeV/{id}", name="commande_validate", methods={"POST"})
     */
    public function validate(Request $request, Commande $commande): Response
    {
        if ($this->isCsrfTokenValid('validate'.$commande->getId(), $request->request->get('_token'))) {
            try{
                $entityManager = $this->getDoctrine()->getManager();
                $commande->setStatus(1);
                $entityManager->persist($commande);
                $entityManager->flush();
                $this->addFlash('success', "Votre commande est enregistrée." );
            }catch(Exception $e){
                $this->addFlash('error', "Votre commande n'a pas pu être enregistrée." );
            }
           
        }
        
        return $this->redirectToRoute('commande', [], Response::HTTP_SEE_OTHER);
    }

}
